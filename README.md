# Booksonic-Podcast-List-JSON

Fetches the podcast list from a Booksonic server and returns it as JSON to be used for display. No episode information is included, only name, URL, description and image. Patreon URLs are censored.